const fs = require("fs");
const rimraf = require("rimraf").sync;
const child_process = require("child_process");
const configAll = require("../config/render_config.json");

let renderers = [];
let configPrime;
let numRenderersFinished = 0;
let highestRenderedFrame = 0;
let lastProgressSent = 0;

process.on("message", msg => renderJob(msg.job));
const renderJob = job => {
	configPrime = configAll[job.type];
	const contentId = getContentId(job);
	const renderPath = configPrime.renderPath + job.msNumber;
	if (fs.existsSync( renderPath )){
		rimraf(renderPath);
	}
	fs.mkdirSync(renderPath);
	for (let i=0; i<configPrime.numProcesses; i++) {
		const renderer = child_process.fork("./lib/child_renderer");
		renderer.on("message", handleRenderMessage);
		renderer.on("exit", handleRenderFinish);
		renderers.push({process:renderer, status:0});
	}
	renderers.forEach((renderer, index) => {
		const config = Object.assign(configPrime, {startFrame:index, rendererNum:index, jobId:job.id, contentId:contentId, msNumber:job.msNumber});
		renderer.process.send({config:config});
	});
};
const handleRenderMessage = m => {
	if (m.message.status > highestRenderedFrame) {
		highestRenderedFrame = m.message.status;
		const renderProgress = highestRenderedFrame / configPrime.totalFrames;
		if (renderProgress - lastProgressSent > 0.01) {
			process.send({status:renderProgress.toFixed(2)});
			lastProgressSent = renderProgress;
		}
	}
};
const handleRenderFinish = () => {
	numRenderersFinished++;
	if (numRenderersFinished >= configPrime.numProcesses) {
		process.exit();
	}
};
const getContentId = job => {
	if (job.type === "tweet") {
		return job.data1;
	} else {
		return job.id;
	}
};
