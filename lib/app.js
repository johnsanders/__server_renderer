const fs = require("fs");
const uid = require("uid");
const renderManager = require("./renderManager");
const encoder = require("./encoder");
const ftpTransfer = require("./ftpTransfer");
const msHelpers = require("./helpers/msHelpers");
const dbHelpers = require("./helpers/dbHelpers");
const fileHelpers = require("./helpers/fileHelpers");
const IpcClient = require("./helpers/IpcClient");

const id = uid();
const ipcClient = new IpcClient(id, true);

const run = async () => {
	ipcClient.setBusy(true);
	const someoneIsBusy = await ipcClient.isAnyoneBusy();
	if (someoneIsBusy) {
		exit();
	} 
	try {
		const jobs = await dbHelpers.getQueuedJobs();
		if (jobs.length > 0) {
			const job = jobs[0];
			fileHelpers.doLog(job);
			try {
				dbHelpers.setStartFinish(job.id, "started");
				await renderManager.prepRender(job);
				await renderManager.doRender(job);
				await renderManager.padToEnd(job);
				await encoder.toMxf(job, dbHelpers.updateStatus);
				dbHelpers.updateStatus(job.id, "Transferring to Mediasource");
				await ftpTransfer(job.msNumber);
				fileHelpers.clearTmpFiles(job);
				ipcClient.setBusy(false);
				try {
					await awaitMs(job);
				} catch (err) {
					await dbHelpers.updateStatus(job.id, "Check Mediasource");
					await dbHelpers.setStartFinish(job.id, "finished");
					console.error(err);
				}
			} catch (err) {
				await dbHelpers.updateStatus(job.id, "Queued");
				console.error(err);
				exit();
			}
		}
	} catch (err) {
		await dbHelpers.updateStatus(job.id, "Queued");
		throw new Error(err);
	}
	exit();
};
const exit = () => {
	ipcClient.disconnect();
	process.exit();
};

const awaitMs = async job => {
	try{
		await dbHelpers.updateStatus(job.id, "Waiting for MS Ingest");
		await msHelpers.awaitMsIngest(job.msNumber, "london");
		await dbHelpers.updateStatus(job.id, "Waiting for ATL Playback Xfer");
		await msHelpers.transferToAtlanta(job.msNumber);
		await msHelpers.awaitMsIngest(job.msNumber, "atlanta");
		await dbHelpers.updateStatus(job.id, "Finished");
		await dbHelpers.setStartFinish(job.id, "finished");
		msHelpers.sendMsConfirmation(job);
	} catch (err) {
		throw new Error(err);
	}
};
run().catch(err => {
	console.error(err);
});
