const child_process = require("child_process");
const ffmpegArgsPrime = require("../config/ffmpeg_args.json");
const renderConfigAll = require("../config/render_config.json");
const regex = /frame=\s*?([0-9]+?)\sfps=/;

module.exports.toMxf = (job, updateStatus) => {
	const updateInterval = 5;
	const jobId = job.id;
	const msNumber = job.msNumber;
	const renderConfig = renderConfigAll[job.type];
	const renderPath = renderConfig.renderPath + msNumber + "/";
	const ffmpegArgs = ffmpegArgsPrime.concat([renderPath + msNumber + ".mxf"]);
	const ffmpegInputIndex = ffmpegArgs.indexOf("INPUT_DIR");
	ffmpegArgs[ffmpegInputIndex] = "./tmp/" + msNumber + "/%04d.png";
	let updateCount = 0;

	const onFfmpegProgress = msg => {
		if (msg.substring(0,6) === "frame=") {
			const result = regex.exec(msg);
			if (result.length > 1 && updateCount >= updateInterval) {
				const framesRendered = parseInt(result[1]);
				const status = (framesRendered / renderConfig.padToFrame).toFixed(2);
				updateStatus(jobId, status);
				updateCount = 0;
			} else {
				updateCount++;
			}
		}
	};
	return new Promise((resolve, reject) => {
		const ffmpeg = child_process.spawn("/usr/local/bin/ffmpeg", ffmpegArgs);
		ffmpeg.stderr.on("data", data => onFfmpegProgress(data.toString()));
		ffmpeg.on("exit", exitStatus => exitStatus === 0 ? resolve() : reject());
	});
};
