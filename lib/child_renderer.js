const fs = require("fs");
const chromeLauncher = require("chrome-launcher");
const CDP = require("chrome-remote-interface");
const leftPad = require("leftpad");

process.on("message", m => run(m));

const launchChrome = (width, height) => (
	chromeLauncher.launch({
		chromePath:"/usr/bin/google-chrome",
		chromeFlags:[
			`--window-size=${width},${height}`,
			"--headless",
			"--no-sandbox"
		],
		logLevel:"error"
	})
);
const run = m => {
	const config = m.config;
	launchChrome(config.width, config.height).then(async chrome => {
		const protocol = await CDP({port:chrome.port});
		try {
			const {Page, Runtime} = protocol;
			await Page.enable();
			Page.navigate({url:config.templateUrl});
			await Page.loadEventFired();
			const loadExpression = `loadContent("${config.contentId}", "${config.stillDuration}");`;
			await Runtime.evaluate({expression:loadExpression, awaitPromise:true});
			const renderPath = config.renderPath + config.msNumber + "/";
			for (let i=config.startFrame; i<=config.totalFrames; i+=config.numProcesses) {
				const time = i * config.frameDuration;
				const updateExpression = `updateTweenTime(${time});`;
				await Runtime.evaluate({expression:updateExpression, awaitPromise:true});
				const {data} = await Page.captureScreenshot({fromSurface:true});
				fs.writeFileSync(renderPath + leftPad(i,4,"0") + ".png", Buffer.from(data, "base64"));
				process.send({message:{rendererNum:config.rendererNum, status:i}});
			}
			protocol.close();
			chrome.kill();
			process.exit();
		} catch (err) {
			console.error(err);
			protocol.close();
			chrome.kill();
			process.exit(1);
		}
	}).catch(err => {
		console.error(err);
		process.exit(1);
	});
};
