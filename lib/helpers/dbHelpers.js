const mysql = require("mysql");
const fileHelpers = require("./fileHelpers");
const dbInfo = require("../../config/db_config");

module.exports.getQueuedJobs = () => {
	const query = "SELECT * from `status_render` WHERE `status` = 'Queued'";
	return new Promise((resolve, reject) => {
		const link = mysql.createConnection(dbInfo);
		link.connect(err => {
			if (err) { reject(err); }
			link.query(query, (err, res) => {
				if (err) { reject(err); }
				link.end();
				resolve(res);
			});
		});
	});
};
module.exports.checkStillstringImages = jobId => {
	const query = `SELECT * from \`stillstring_images\` WHERE \`string_id\` = '${jobId}'`;
	const imageDirectory = "http://cnnitouch.turner.com/utilities/getty_importer/tmp/";
	return new Promise((resolve, reject) => {
		const link = mysql.createConnection(dbInfo);
		link.connect(err => {
			if (err) { reject(err); }
			link.query(query, (err, res) => {
				if (err) { reject(err); }
				link.end();
				Promise.all(res.map(imageInfo => fileHelpers.checkFileExists(imageDirectory, imageInfo.filename)))
					.then(result => resolve(result.indexOf(false) === -1))
					.catch(err => console.error(err));
			});
		});
	});
};
module.exports.updateStatus = (jobId, status) => {
	return new Promise( (resolve, reject) => {
		const query = "UPDATE `status_render` SET `status`='" + status + "' WHERE `id` = '" + jobId + "'";
		const link = mysql.createConnection(dbInfo);
		link.connect(err => {
			if (err) { reject(err); }
			link.query(query, (err) => {
				if (err) { reject(err); }
				link.end();
				resolve()
			});
		});
	})
};

module.exports.setStartFinish = (jobId, startFinish) => {
	const query = "UPDATE `status_render` SET `" + startFinish + "`=NOW() WHERE `id` = '" + jobId + "'";
	return new Promise((resolve, reject) => {
		const link = mysql.createConnection(dbInfo);
		link.connect((err) => {
			if (err) { reject(err); }
			link.query(query, (err) => {
				if (err) { reject(err); }
				link.end();
				resolve();
			});
		});
	});
};

