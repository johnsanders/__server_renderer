const http = require("http");
const os = require("os");
const sendmail = require("sendmail")({silent:true});
const bmamHost = "cnnitouch.turner.com";
const typeNames = {
	tweet: "Tweet to Air",
	stillstring: "StillStringer"
};

module.exports.awaitMsIngest = (msNumber, loc) => {
	return new Promise(async (resolve, reject) => {
		let count = 0;
		const checkOnIngest = async () => {
			try {
				const data = await getUrl("/utilities/bmam/getMsInfo.php", `id=${msNumber}&loc=${loc}`);
				if (
					data.payload
					&& data.payload.items
					&& data.payload.items.length === 1
					&& data.payload.items[0].importStatus === "READY"
				) {
					resolve();
					setTimeout(resolve, 20);
				} else if (count < 20) {
					count++;
					setTimeout(checkOnIngest, 30000);
				} else { reject("Giving up on MS ingest"); }
			} catch (err) { reject(err); }
		};
		checkOnIngest();
	});
};

module.exports.transferToAtlanta = msNumber => {
	return new Promise(async (resolve, reject) => {
		if (String(msNumber).match(/[0-9]{8}/)) {
			try {
				// WAIT 20 SECONDS TO GIVE THUMBS TIME TO GENERATE
				setTimeout(async () => {
					await getUrl("/utilities/bmam/sendToAtlantaPlayback.php", `id=${msNumber}`);
					resolve();
				}, 20000);
			} catch (err) { reject(err); }
		} else {
			reject("Not a valid MS Number");
		}
	});
};

module.exports.sendMsConfirmation = job => {
	const host = process.env.HOSTNAME || os.hostname() || "cnnitouch";
	const to = job.email.replace("@cnn.com", "@turner.com");
	const from = "\"John Sanders <john.sanders@turner.com>\"";
	const renderType = typeNames[job.type];
	const html = `Hello!  Your ${renderType} render of ${job.name} ` +
		`is now available in Atlanta playback on MS number ${job.msNumber}.\n` +
		"Let me know if you have any problems with it. \n\n" + 
		"Rendered on server: " + host;
	const opts = {
		from:"John Sanders <john.sanders@turner.com>",
		subject:"Your " + renderType + " render",
		to, html
	};
	const optsSendToMe = Object.assign({}, opts, {to:"john.sanders@turner.com"});
	sendmail(opts);
	sendmail(optsSendToMe);
};

const getUrl = (bmamPath, args) => {
	return new Promise((resolve, reject) => {
		const options = {
			host: bmamHost,
			path: bmamPath + "?" + args
		};
		try {
			const req = http.request(options, (res) => {
				let raw = "";
				res.on("data", data => raw += data);
				res.on("end", () => {
					try {
						const json = JSON.parse(raw);
						resolve(json.response);
					} catch(e) {
						reject("Error fetching json from " + bmamPath + ". Got: " + raw);
					}
				});
			});
			req.on("error", e => reject(e.message));
			req.end();
		} catch (err) { reject(err); }
	});
};
