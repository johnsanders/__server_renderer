const fs = require("fs");
const http = require("http");
const url = require("url");
const axios = require("axios");
const leftPad = require("leftpad");
const rimraf = require("rimraf").sync;
const renderConfigAll = require("../../config/render_config.json");
const logFile = "/home/cnnitouch/render.log";

module.exports.checkFileExists = (baseUrl, filename) => {
	return new Promise((resolve, reject) => {
		if (!filename) {
			resolve(false);
		}
		const fileUrl = baseUrl + filename;
		const options = {
			method: "HEAD",
			host: url.parse(fileUrl).host,
			port: 80,
			path: url.parse(fileUrl).pathname
		};
		http.request(options, res => {
			if (res.statusCode === 200) {
				resolve(true);
			} else {
				resolve(false);
			}
		}).on("error", err => reject(err)).end();
	});
};
module.exports.downloadImages = jobId => {
	const downloaderUrl = "http://cnnitouch.turner.com/apps/stillstringer/server/downloadImages.php?id=" + jobId;
	return new Promise((resolve, reject) => {
		axios.get(downloaderUrl)
			.then(res => resolve(res.data))
			.catch(err => reject(err));
	});
};
module.exports.padLastFrame = (job, frameToPad, padToFrame) => {
	const renderPath = renderConfigAll[job.type].renderPath + job.msNumber + "/";
	return new Promise((resolve, reject) => {
		const sourceFilename = numToFilename(frameToPad);
		const sourceFile = fs.readFileSync(renderPath + sourceFilename);
		for (let count = frameToPad + 1; count <= padToFrame; count++) {
			const destFilename = numToFilename(count);
			try {
				fs.writeFileSync(renderPath + destFilename, sourceFile);
			} catch (err) { reject(err); }
		}
		resolve();
	});
};
const numToFilename = num => {
	return leftPad(num.toString(), 4, "0") + ".png";
};
module.exports.clearTmpFiles = job => {
	const msNumber = job.msNumber;
	const renderPath = renderConfigAll[job.type].renderPath;
	rimraf(renderPath + msNumber);
};
module.exports.doLog = job => {
	const string = job.type + " " + job.email + " " + job.name + " " + Date.now().toString();
	fs.appendFile(logFile, string + "\n", () => {});
};
