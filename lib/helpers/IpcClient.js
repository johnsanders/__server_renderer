const ipc = require("node-ipc");
ipc.config.silent = true;

class IpcClient {
	constructor(id, connectImmediately){
		this.id = id;
		this.busy = false;
		if (connectImmediately){
			this.connect();
		}
	}
	connect(){
		ipc.connectTo("ipc", () => {
			ipc.of.ipc.on("areYouBusy", () => {
				if (this.busy) {
					ipc.of.ipc.emit("iAmBusy", this.id);
				}
			});
		});
	}
	disconnect(){
		ipc.disconnect("ipc");
	}
	isAnyoneBusy(){
		return new Promise( (resolve, reject) => {
			const timeout = setTimeout( () => resolve(false), 2000 );
			ipc.of.ipc.on("someoneIsBusy", id => {
				if (id !== this.id) {
					clearTimeout(timeout);
					ipc.of.ipc.off("someoneIsBusy", "*");
					resolve(true)
				}
			});
			ipc.of.ipc.emit("isAnyoneBusy");
		});
	}
	setBusy(busy) {
		this.busy = busy;
	}
}

module.exports = IpcClient;
