const Ftp = require("ftp");
const ftpInfo = require("../config/ftp_config.json").live;
const renderPath = require("../config/render_config.json").renderPath;

module.exports = msNumber => {
	return new Promise((resolve, reject) => {
		const ftp = new Ftp();
		ftp.on("ready", () => {
			const path = renderPath + msNumber + "/";
			ftp.put(path + msNumber + ".mxf", msNumber + ".mxf", (err) => {
				if (err) {
					reject(err);
				} else {
					ftp.end();
					resolve();
				}
			});
		});
		ftp.connect(ftpInfo);
	});
};
