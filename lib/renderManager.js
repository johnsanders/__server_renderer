const child_process = require("child_process");
const dbHelpers = require("./helpers/dbHelpers");
const fileHelpers = require("./helpers/fileHelpers");
const renderConfig = require("../config/render_config");

module.exports.prepRender = job => {
	if (job.type === "stillstring") {
		return prepStillstring(job);
	} else {
		return Promise.resolve();
	}
};
const prepStillstring = job => {
	return new Promise(async (resolve, reject) => {
		try {
			const imagesReady = await dbHelpers.checkStillstringImages(job.id);
			if (imagesReady) {
				resolve();
			} else {
				dbHelpers.updateStatus(job.id, "Downloading Images");
				try {
					await fileHelpers.downloadImages(job.id);
					resolve();
				} catch (err) { reject(err); }
			}
		} catch(err) { reject(err); }
	});
};
module.exports.doRender = job => {
	return new Promise((resolve, reject) => {
		try {
			dbHelpers.updateStatus(job.id, "Starting Render");
			const renderer = child_process.fork("./lib/renderer");
			renderer.on("message", msg => dbHelpers.updateStatus(job.id, msg.status));
			renderer.on("exit",	exitStatus => exitStatus === 0 ? resolve() : reject());
			renderer.send({job:job});
		} catch (err) { reject(err); }
	});
};

module.exports.padToEnd = job => {
	const framesRendered = renderConfig[job.type].totalFrames;
	const padToFrame = renderConfig[job.type].padToFrame;
	if (framesRendered < padToFrame) {
		return fileHelpers.padLastFrame(job, framesRendered, padToFrame);
	} else {
		return Promise.resolve();
	}
};
